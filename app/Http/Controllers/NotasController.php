<?php

namespace App\Http\Controllers;

use App\Notas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\Promise\all;

class NotasController extends Controller
{
    //todos menos paciente
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('RolAdmin:3');
    }
    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notas=Notas::join('users','notas.id_Emisor','users.id')
        ->select('notas.*','Nombre')->get();
        return view('Notas.index',compact('notas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $notas=DB::table('users')->join('rol_permisos','users.id','rol_permisos.user_id')->where('rol_id','3')->select('id','Nombre')->get();
        return view('Notas.create',compact('notas'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         Notas::insert([
            'Id_Receptor' => $request['Id_Receptor'],
             'Id_Emisor' => $request['Id_Emisor'],
            'Asunto' => $request['Asunto'],
             'Descripcion' => $request['Descripcion'],
             'created_at' => today(),
         ]);
         return redirect()->route('notas.index')
        ->with('success','Nota creada exitosamente.');
         return view('Notas.create',compact('notas'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notas  $notas
     * @return \Illuminate\Http\Response
     */
    public function show(Notas $notas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notas  $notas
     * @return \Illuminate\Http\Response
     */
    public function edit(Notas $notas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notas  $notas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notas $notas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notas  $notas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notas $notas)
    {
        //
    }
}
