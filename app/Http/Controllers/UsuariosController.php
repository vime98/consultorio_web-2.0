<?php

namespace App\Http\Controllers;

use App\Rol;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('RolAdmin:1');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::join('rol_permisos','users.id','rol_permisos.user_id')
        ->join('rol','rol_permisos.rol_id','rol.id')
        ->select('users.*')->distinct('')->get();


        return view('auth.consultar',compact('users'));
        // $users=User::find(2);
        // $users->roles;
        // dd($users);
        // return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        User::insert([
            'Nombre' => $request['Nombre'],
            'Apellido_Paterno' => $request['Apellido_Paterno'],
            'Apellido_Materno' => $request['Apellido_Materno'],
            'Edad' => $request['Edad'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'created_at' => today(),
        ]);
        $id=DB::table('users')->select('id')->max('id');
        DB::table('rol_permisos')->insert([
            'rol_id'=>'4',
            'user_id'=>$id,
            'created_at'=> today(),
        ]);
        return redirect()->route('usuarios.index')
        ->with('success','Usuario creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show( )
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::findOrfail($id);
        $roles = DB::table('rol')->select('id','Nombre_rol')->get();

        return view('auth.edit',compact('users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::findorfail($id);
        $users->Nombre =  $request->Nombre;
        $users->Apellido_Paterno =  $request->Apellido_Paterno;
        $users->Apellido_Materno = $request->Apellido_Materno;
        $users->Edad =  $request->Edad;
        $users->email =  $request->email;
        $users->save();

        return redirect()->route('usuarios.index')
                        ->with('success','Usuario actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('usuarios.index')
                        ->with('success','Product deleted successfully');
    }
}
