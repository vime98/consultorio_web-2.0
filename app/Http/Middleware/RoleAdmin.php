<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class RoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::guard('paciente')) {
            return $next($request);
        }
        else
        $usuario = Auth::user();
        $usuario->id;
        $usuario->roles;
        foreach ($usuario->roles as $rol) {
            switch ($role) {
                case '1':
                    if ($rol->Nombre_rol == "Administrador") {
                        return $next($request);
                    }
                    break;
                case '2':
                    if ($rol->Nombre_rol == "Administrador" ||$rol->Nombre_rol == "Doctores") {
                        return $next($request);
                    }
                    break;
                case '3':
                    if ($rol->Nombre_rol == "Administrador" ||$rol->Nombre_rol == "Doctores"||$rol->Nombre_rol == "Secretaria") {
                        return $next($request);
                    }
                    break;
                case '4':
                    if ($rol->Nombre_rol == "Administrador" ||$rol->Nombre_rol == "Doctores") {
                            return $next($request);
                    }
                    elseif ($rol->Nombre_rol == "Secretaria") {
                        return redirect(route('home'));
                    }
                    elseif (Auth::guard('paciente')->user()->isPaciente) {
                        return $next($request);
                    }
                    break;
                default:
                    break;
            }

        }
        return redirect(route('home'));
        /*dd($request->user()->hasRole($role));
        if (! $request->user()->has_role($role)) {
            return redirect('home');
        }
        else return $next($request);*/
    }
}
