<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleDoc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = Auth::user();
        $usuario->id;
        $usuario->roles;
        foreach ($usuario->roles as $role) {

            if ($role->Nombre_rol == "Doctores") {
                return $next($request);
            }
        }

        return redirect(route('home'));
    }
}
