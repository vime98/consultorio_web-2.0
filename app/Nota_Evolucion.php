<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota_Evolucion extends Model
{
    //
    protected $table = 'nota_evolucion';
    protected $fillable = [
        'paciente_id','doctor_id','Sintomas','Diagnostico','Tratamiento','Signos',
    ];

    public function usr()
    {
        return $this->belongsTo('App\User','doctor_id','id');
    }

    public function pac()
    {
        return $this->belongsTo('App\Pacientes','paciente_id','id');
    }


}
