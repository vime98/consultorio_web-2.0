<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
      

        Schema::create('rol_permisos', function (Blueprint $table){
            $table->unsignedBigInteger('rol_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('rol_id')
                ->references('id')
                ->on('rol')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

                $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               

        Schema::drop('rol_permisos');
       
    }
}
