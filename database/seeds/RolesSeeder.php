<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\This;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insertar roles
        DB::table('rol')->insert([
            'Nombre_rol' => 'Administrador',
          ]);
        DB::table('rol')->insert([
            'Nombre_rol' => 'Doctores',
          ]);
        DB::table('rol')->insert([
            'Nombre_rol' => 'Secretaria',
          ]);
          DB::table('rol')->insert([
            'Nombre_rol' => 'Sin_Rol',
          ]);

          $faker = Faker::create();
        //Insertar doctor y admin
        DB::table('users')->insert([
            'Nombre' => $faker->name,
            'Apellido_Paterno' => $faker->lastname,
            'Apellido_Materno' => $faker->lastname,
            'Edad' => $faker->numberBetween(20,70),
            'email' => 'doctor@gmail.com',
            'password' => Hash::make('password'),
            'created_at' => today(),
          ]);

        DB::table('users')->insert([
            'Nombre' => $faker->name,
            'Apellido_Paterno' => $faker->lastname,
            'Apellido_Materno' => $faker->lastname,
            'Edad' => $faker->numberBetween(20,70),
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'created_at' => today(),
          ]);

          DB::table('users')->insert([
            'Nombre' => $faker->name,
            'Apellido_Paterno' => $faker->lastname,
            'Apellido_Materno' => $faker->lastname,
            'Edad' => $faker->numberBetween(20,70),
            'email' => 'secretaria@gmail.com',
            'password' => Hash::make('password'),
            'created_at' => today(),
          ]);


        //Paciente
        DB::table('pacientes')->insert([
            'Nombre' => $faker->name,
            'Apellido_Paterno' => $faker->lastname,
            'Apellido_Materno' => $faker->lastname,
            'Sexo' => "F",
            'email' => 'paciente1@gmail.com',
            'password' => Hash::make('password'),
            'compartido' => '1',
            'user_id' => '1',
            'isPaciente' => '1',
            'created_at' => today(),
          ]);

          //Notas_Secretaria
          DB::table('notas')->insert([
            'Id_Receptor' => '3',
            'Id_Emisor' => '1',
            'Asunto' => 'El cafe',
            'Descripcion' => 'Necesito un cafe',
            'created_at' => today(),
          ]);

          DB::table('rol_permisos')->insert([
            'rol_id' => '4',
            'user_id' => '1',
            'created_at' => today(),
            'updated_at' => today(),

          ]);

          DB::table('rol_permisos')->insert([
            'rol_id' => '4',
            'user_id' => '2',
            'created_at' => today(),
            'updated_at' => today(),

          ]);
          DB::table('rol_permisos')->insert([
            'rol_id' => '4',
            'user_id' => '3',
            'created_at' => today(),
            'updated_at' => today(),

          ]);
    }
}
