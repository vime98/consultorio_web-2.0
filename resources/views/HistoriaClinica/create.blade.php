@extends('layouts.app')
@section('content')
<br>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Agregar Historia Clinica</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('VerExpe',$paciente->id) }}"> Atras</a>
        </div>
    </div>
</div>

<form action="{{ route('historia_clinica.store') }}" method="POST">
    @csrf
     <div class="form-group">
         <br>
        <div class="form-group">
            <label for="Nombre">{{'Nombre'}}</label>
            <input type="text" name="Nombre" id="Nombre" class="form-control input-sm" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{3,40}"
            value="{{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}}">
            @if ($errors->has('Nombre'))
                <span class="text-danger">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-2 mx-auto">
                <label for="Edad">{{'Edad'}}</label>
                <input type="text" name="Edad" id="Edad" class="form-control @error('Edad') is-invalid @enderror input-sm" pattern="[0-9]{1,3}"
                value="{{ old('Edad') }}" required>
                @if ($errors->has('Edad'))
                    <span class="text-danger">{{ $errors->first('Edad') }}</span>
                @elseif($errors->has('Direccion'))
                <span class="text-danger"></span>
                @endif
            </div>
            <div class="col-md-10 mx-auto">
                <label for="Direccion">{{'Direccion'}}</label>
                <input type="text" name="Direccion" id="Direccion" class="form-control @error('Direccion') is-invalid @enderror input-sm" pattern="[a-z0-9A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð #,.'-]{3,100}"
                value="{{ old('Direccion') }}" required>
                @if ($errors->has('Direccion'))
                    <span class="text-danger">{{ $errors->first('Direccion') }}</span>
                @elseif($errors->has('Edad'))
                <span class="text-danger"></span>
                @endif
            </div>
        </div>
        <br>
        <p><strong> Antecedentes heredofamiliares</strong></p>
        <div class="form-group">
            <label for="Ante_AbuelosP">{{'_Abuelos Paternos'}}</label>
            <input type="text" name="Ante_AbuelosP" id="Ante_AbuelosP" class="form-control input-sm"
            value="{{ old('Ante_AbuelosP') }}">
        </div>
        <div class="form-group">
            <label for="Ante_AbuelosM">{{'_Abuelos Maternos'}}</label>
            <input type="text" name="Ante_AbuelosM" id="Ante_AbuelosM" class="form-control input-sm"
            value="{{ old('Ante_AbuelosM') }}">
        </div>
        <div class="form-group">
            <label for="Ante_Tios">{{'_Tios'}}</label>
            <input type="text" name="Ante_Tios" id="Ante_Tios" class="form-control input-sm"
            value="{{ old('Ante_Tios') }}">
        </div>
        <div class="form-group">
            <label for="Ante_Mama">{{'_Mama'}}</label>
            <input type="text" name="Ante_Mama" id="Ante_Mama" class="form-control input-sm"
            value="{{ old('Ante_Mama') }}">
        </div>
        <div class="form-group">
            <label for="Ante_Papa">{{'_Papa'}}</label>
            <input type="text" name="Ante_Papa" id="Ante_Papa" class="form-control input-sm"
            value="{{ old('Ante_Papa') }}">
        </div>
        <div class="form-group">
            <label for="Ante_Hermanos">{{'_Hermanos'}}</label>
            <input type="text" name="Ante_Hermanos" id="Ante_Hermanos" class="form-control input-sm"
            value="{{ old('Ante_Hermanos') }}">
        </div>
        <p><strong>Antecedentes Personales No Patologicos</strong></p>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-3 mx-auto">
                <label for="Fecha_Nacimiento">{{'Fecha Nacimiento'}}</label>
                <input type="Date" name="Fecha_Nacimiento" id="Fecha_Nacimiento" class="form-control input-sm"
                value="{{ old('Fecha_Nacimiento') }}">
                @if ($errors->has('Fecha_Nacimiento'))
                    <span class="text-danger">{{ $errors->first('Fecha_Nacimiento') }}</span>
                @endif
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Lugar_Nacimiento">{{'Lugar Nacimiento'}}</label>
                <input type="text" name="Lugar_Nacimiento" id="Lugar_Nacimiento" class="form-control input-sm" pattern="[a-z0-9A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð #,.'-]{3,100}"
                value="{{ old('Lugar_Nacimiento') }}">
                @if ($errors->has('Fecha_Nacimiento'))
                <span class="text-danger"></span>
                @endif
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Estado_Civil">{{'Estado Civil'}}</label>
                <select id="Estado_Civil"  class="form-control" name="Estado_Civil" autofocus>
                    <option value="Soltero">Soltero</option>
                    <option value="Casado">Casado</option>
                    <option value="Viudo">Viudo</option>
                </select>
                @if ($errors->has('Fecha_Nacimiento'))
                <span class="text-danger"></span>
                @endif
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Toxicomanias">{{'Toxicomanias'}}</label>
                <input type="text" name="Toxicomanias" id="Toxicomanias" class="form-control input-sm" pattern="[a-z0-9A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð #,.'-]{3,100}"
                value="{{ old('Toxicomanias') }}">
                @if ($errors->has('Toxicomanias'))
                <span class="text-danger"></span>
                @endif
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="Residencia_actual">{{'_Residencia Actual'}}</label>
            <input type="text" name="Residencia_actual" id="Residencia_actual" class="form-control input-sm"
            value="{{ old('Residencia_actual') }}">
        </div>
        <div class="form-group">
            <label for="Escolaridad">{{'_Escolaridad'}}</label>
            <input type="text" name="Escolaridad" id="Escolaridad" class="form-control input-sm"
            value="{{ old('Escolaridad') }}">
        </div>
        <div class="form-group">
            <label for="Habitos_Higiene">{{'_Habitos de Higiene'}}</label>
            <input type="text" name="Habitos_Higiene" id="Habitos_Higiene" class="form-control input-sm"
            value="{{ old('Habitos_Higiene') }}">
        </div>
        <div class="form-group">
            <label for="Habitos_Dieta">{{'_Habitos de Dieta'}}</label>
            <input type="text" name="Habitos_Dieta" id="Habitos_Dieta" class="form-control input-sm"
            value="{{ old('Habitos_Dieta') }}">
        </div>
        <p><strong>Antecedentes Personales Patologicos</strong></p>
        <div class="form-group">
            <label for="Enferme_infancia">{{'Enfermedad de la Infancia'}}</label>
            <input type="text" name="Enferme_infancia" id="Enferme_infancia" class="form-control input-sm"
            value="{{ old('Enferme_infancia') }}">
        </div>
        <div class="form-group">
            <label for="Cirugias">{{'Cirugias'}}</label>
            <input type="text" name="Cirugias" id="Cirugias" class="form-control input-sm"
            value="{{ old('Cirugias') }}">
        </div>
        <div class="form-group">
            <label for="Alergias">{{'Alergias'}}</label>
            <input type="text" name="Alergias" id="Alergias" class="form-control input-sm"
            value="{{ old('Alergias') }}">
        </div>
        <div class="form-group">
            <label for="Medica_Actual">{{'Medicamento que toma Actualmente'}}</label>
            <input type="text" name="Medica_Actual" id="Medica_Actual" class="form-control input-sm"
            value="{{ old('Medica_Actual') }}">
        </div>
        <p><strong>Antecedentes Gineco Obstetricos</strong></p>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-3 mx-auto">
                <label for="Menarca">{{'Menarca'}}</label>
                <input type="Text" name="Menarca" id="Menarca" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Menarca') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Ritmo_Menstrual">{{'Ritmo Menstrual'}}</label>
                <input type="text" name="Ritmo_Menstrual" id="Ritmo_Menstrual" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Ritmo_Menstrual') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="FUM">{{'FUM'}}</label>
                <input type="text" name="FUM" id="FUM" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('FUM') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Gestas">{{'Gestas'}}</label>
                <input type="text" name="Gestas" id="Gestas" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Gestas') }}">
            </div>
        </div>
        <br>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-3 mx-auto">
                <label for="Abortos">{{'Abortos'}}</label>
                <input type="Text" name="Abortos" id="Abortos" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Abortos') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Cesareas">{{'Cesareas'}}</label>
                <input type="text" name="Cesareas" id="Cesareas" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Cesareas') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="FUP">{{'FUP'}}</label>
                <input type="text" name="FUP" id="FUP" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('FUP') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="USA">{{'USA'}}</label>
                <input type="text" name="USA" id="USA" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('USA') }}">
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="Anticonseptivos">{{'Uso de Anticonseptivos'}}</label>
            <input type="text" name="Anticonseptivos" id="Anticonseptivos" class="form-control input-sm"
            value="{{ old('Anticonseptivos') }}">
        </div>
        <div class="form-group">
            <label for="Paras">{{'Paras'}}</label>
            <input type="text" name="Paras" id="Paras" class="form-control input-sm"
            value="{{ old('Paras') }}">
        </div>
        <p><strong>Exploracion</strong></p>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-3 mx-auto">
                <label for="Peso">{{'Peso'}}</label>
                <input type="Text" name="Peso" id="Peso" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Peso') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Talla">{{'Talla'}}</label>
                <input type="text" name="Talla" id="Talla" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Talla') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Cabeza">{{'Cabeza'}}</label>
                <input type="text" name="Cabeza" id="Cabeza" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Cabeza') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Ojos">{{'Ojos'}}</label>
                <input type="text" name="Ojos" id="Ojos" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Ojos') }}">
            </div>
        </div>
        <br>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-3 mx-auto">
                <label for="Oidos">{{'Oidos'}}</label>
                <input type="Text" name="Oidos" id="Oidos" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Oidos') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Nariz">{{'Nariz'}}</label>
                <input type="text" name="Nariz" id="Nariz" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Nariz') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Torax">{{'Torax'}}</label>
                <input type="text" name="Torax" id="Torax" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Torax') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Forma">{{'Forma'}}</label>
                <input type="text" name="Forma" id="Forma" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Forma') }}">
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="Boca">{{'Boca'}}</label>
            <input type="text" name="Boca" id="Boca" class="form-control input-sm"
            value="{{ old('Boca') }}">
        </div>
        <div class="form-group">
            <label for="Cuello">{{'Cuello'}}</label>
            <input type="text" name="Cuello" id="Cuello" class="form-control input-sm"
            value="{{ old('Cuello') }}">
        </div>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-3 mx-auto">
                <label for="Mov_Respira">{{'Moviemientos Respiratorios'}}</label>
                <input type="Text" name="Mov_Respira" id="Mov_Respira" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Mov_Respira') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="FR">{{'FR'}}</label>
                <input type="text" name="FR" id="FR" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('FR') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="Ruido_Anormal">{{'Ruidos Anormales'}}</label>
                <input type="text" name="Ruido_Anormal" id="Ruido_Anormal" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('Ruido_Anormal') }}">
            </div>
            <div class="col-md-3 mx-auto">
                <label for="FC">{{'FC'}}</label>
                <input type="text" name="FC" id="FC" class="form-control input-sm" pattern="[0-9 ,-.'/]{0,5}"
                value="{{ old('FC') }}">
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="Abdomen">{{'Abdomen'}}</label>
            <input type="text" name="Abdomen" id="Abdomen" class="form-control input-sm"
            value="{{ old('Abdomen') }}">
        </div>
        <div class="form-group">
            <label for="Extre_superior">{{'Extremidades Superiores'}}</label>
            <input type="text" name="Extre_superior" id="Extre_superior" class="form-control input-sm"
            value="{{ old('Extre_superior') }}">
        </div>
        <div class="form-group">
            <label for="Extre_inferior">{{'Extremidades Inferiores'}}</label>
            <input type="text" name="Extre_inferior" id="Extre_inferior" class="form-control input-sm"
            value="{{ old('Extre_inferior') }}">
        </div>
        <input type="hidden" name="pacientes_id" value="{{ $paciente->id }}">
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection
