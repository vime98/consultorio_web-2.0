@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form action="{{route('notas.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="Asunto" class="col-md-4 col-form-label text-md-right">{{ __('Asunto') }}</label>

                            <div class="col-md-6">
                                <input id="Asunto" type="text" class="form-control @error('Asunto') is-invalid @enderror" name="Asunto" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>
                            <div class="col-md-6 ">
                            <textarea name="Descripcion" id="Descripcion" cols="30" rows="10" type="text" class="form-control @error('Descripcion') is-invalid @enderror"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Id_Receptor" class="col-md-4 col-form-label text-md-right ">{{ __(' Secretaria') }}</label>

                            <div class="col-md-6">
                                <select id="Id_Receptor"  class="form-control" name="Id_Receptor" autofocus>
                                    @foreach ($notas as $secre)
                                      <option value={{$secre->id}}>{{$secre->Nombre}}</option> 
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="Id_Emisor" value="{{ Auth::user()->id }}">                    
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                <a class="btn btn-outline-danger" href="{{route('usuarios.index')}}">Regresar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
