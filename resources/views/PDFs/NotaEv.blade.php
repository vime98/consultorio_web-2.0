<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
    <body>
    <style>

        .card {
            flex-direction: column;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }
        .card-body {
            flex: 1 1 auto;
            min-height: 1px;
            padding: 1.25rem;
        }
        .text-white {
            color: #fff !important;
        }
        .bg-dark {
            background-color: #343a40 !important;
        }
        .mb-3,
        .my-3 {
            margin-bottom: 1rem !important;
        }
        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header:first-child {
            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
        }

        .card-header+.list-group .list-group-item:first-child {
            border-top: 0;
        }
        .card-text:last-child .p{
            margin-bottom: 0;
            margin-top:0;

        }
        .card-header:first-child {
            border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
        }
        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }
        .card-body {
            flex: 1 1 auto;
            min-height: 1px;
            padding: 1.25rem;
        }
        * , *::before, *::after{
            box-sizing: border-box;
        }
        .align-items-center {
            align-items: center !important;
        }
        .justify-content-between {
            justify-content: space-between !important;
        }
        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .ml-auto, .mx-auto {
            margin-left: auto !important;
        }
        .mr-auto, .mx-auto {
            margin-right: auto !important;
        }

        .form-group {
            margin-bottom: 1rem;
        }
        html {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }
        .col-xl, .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg, .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md, .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm, .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col, .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }
        body {
            margin: 0;
            font-family: "Nunito", sans-serif;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1.6;
            text-align: left;
            background-color: #f8fafc;
        }

    </style>
    <header>
        <h1>Nota de: <strong>{{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}} </strong></h1>
      </header>

      <div class="form-group">
        <br>
        <div class="card text-white bg-dark mb-3" >
           <div class="card-header">Datos Generales</div>
           <div class="card-body">
                <div class="form-group">
                <p> Nota evolutiva diagnosticada por: <strong>{{$Doc_di->Nombre}} {{$Doc_di->Apellido_Paterno}} {{$Doc_di->Apellido_Materno}}<strong><p>
                 </div>
               <div class="form-group">
                   <p class="card-text"> Sintomas: {{ $nota->Sintomas}} </p>
               </div>
               <div class="form-group">
                <p class="card-text"> Diagnostico: {{ $nota->Diagnostico}}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Tratamiento: {{ $nota->Tratamiento}}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Signos: {{ $nota->Signos}}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Fecha de esta consulta: {{ $nota->created_at->toFormattedDateString()}}</p>
                </div>
                <div class="form-group">
                    <!--{{ $mytime = Carbon\Carbon::now()}}-->
                    <p class="card-text"> Fecha de impresion : {{$mytime->toFormattedDateString()}}</p>
                </div>
                <div class="form-group">
                    <p class="card_text">Doctor Principal : {{ $paciente->user->Nombre }} {{ $paciente->user->Apellido_Paterno }} {{ $paciente->user->Apellido_Materno }}</p>
                </div>

           </div>
         </div>
</body>
</html>
