<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
    <body>
    <style>

        .card {
            flex-direction: column;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }
        .card-body {
            flex: 1 1 auto;
            min-height: 1px;
            padding: 1.25rem;
        }
        .text-white {
            color: #fff !important;
        }
        .bg-dark {
            background-color: #343a40 !important;
        }
        .mb-3,
        .my-3 {
            margin-bottom: 1rem !important;
        }
        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header:first-child {
            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
        }

        .card-header+.list-group .list-group-item:first-child {
            border-top: 0;
        }
        .card-text:last-child .p{
            margin-bottom: 0;
            margin-top:0;

        }
        .card-header:first-child {
            border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
        }
        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }
        .card-body {
            flex: 1 1 auto;
            min-height: 1px;
            padding: 1.25rem;
        }
        * , *::before, *::after{
            box-sizing: border-box;
        }
        .align-items-center {
            align-items: center !important;
        }
        .justify-content-between {
            justify-content: space-between !important;
        }
        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .ml-auto, .mx-auto {
            margin-left: auto !important;
        }
        .mr-auto, .mx-auto {
            margin-right: auto !important;
        }

        .form-group {
            margin-bottom: 1rem;
        }
        html {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }
        .col-xl, .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg, .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md, .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm, .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col, .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }
        body {
            margin: 0;
            font-family: "Nunito", sans-serif;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1.6;
            text-align: left;
            background-color: #f8fafc;
        }

    </style>
    <header>
        <h1>Reporte de {{$paciente->Nombre}}</h1>
      </header>

      <div class="form-group">
        <br>
        <div class="card text-white bg-dark mb-3" >
           <div class="card-header">Datos Generales</div>
           <div class="card-body">
               <div class="form-group">
                   <p class="card-text"> Nombre: {{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}} </p>
               </div>
               <div class="form-group">
                <p class="card-text"> Edad: {{$paciente->Edad}}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Direccion: {{ $paciente->Direccion }}</p>
                </div>
           </div>
         </div>
       <br>
       <div class="card text-white bg-dark mb-3" >
           <div class="card-header"> Antecedentes heredofamiliares</div>
           <div class="card-body">
               <div class="form-group">
                   <p class="card-text"> _Abuelos Paternos: {{ $paciente->Ante_AbuelosP }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Abuelos Maternos: {{ $paciente->Ante_AbuelosM }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Tios: {{ $paciente->Ante_Tios }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Mama: {{ $paciente->Ante_Mama }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Papa: {{ $paciente->Ante_Papa }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Hermanos: {{ $paciente->Ante_Hermanos }}</p>
               </div>
           </div>
         </div>
       <div class="card text-white bg-dark mb-3" >
           <div class="card-header"> Antecedentes Personales No Patologicos</div>
           <div class="card-body">
               <div class="form-group">
                <p class="card-text"> Fecha Nacimiento: {{ $paciente->Fecha_Nacimiento }}</p>
               </div>
               <div class="form-group">
                <p class="card-text"> Lugar Nacimiento: {{ $paciente->Lugar_Nacimiento }}</p>
               </div>
               @if ($paciente->Estado_Civil == "Soltero")
                   <div class="form-group">
                       <p class="card-text"> Estado Civil: Soltero </p>
                   </div>
                   @elseif($paciente->Estado_Civil == "Casado")
                   <div class="form-group">
                       <p class="card-text"> Estado Civil: Casado </p>
                   </div>
                   @else
                   <div class="form-group">
                       <p class="card-text"> Estado Civil: Viudo </p>
                   </div>
                   @endif
                   <div class="form-group">
                       <p class="card-text"> Toxicomanias: {{ $paciente->Toxicomanias }}</p>
                   </div>
               <div class="form-group">
                   <p class="card-text"> _Residencia Actual: {{ $paciente->Residencia_actual }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Escolaridad: {{ $paciente->Escolaridad }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Habitos de Higiene: {{ $paciente->Habitos_Higiene }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> _Habitos de Dieta: {{ $paciente->Habitos_Dieta }}</p>
               </div>
           </div>
         </div>
       <div class="card text-white bg-dark mb-3" >
           <div class="card-header"> Antecedentes Personales Patologicos</div>
           <div class="card-body">
               <div class="form-group">
                   <p class="card-text"> Enfermedad de la Infancia: {{ $paciente->Enferme_infancia }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Cirugias: {{ $paciente->Cirugias }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Alergias: {{ $paciente->Alergias }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Medicamento que toma Actualmente: {{ $paciente->Medica_Actual }}</p>
               </div>
           </div>
         </div>
       <div class="card text-white bg-dark mb-3" >
           <div class="card-header"> Antecedentes Gineco Obstetricos</div>
           <div class="card-body">
            <div class="form-group">
                <p class="card-text"> Menarca: {{ $paciente->Menarca }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Ritmo Menstrual: {{ $paciente->Ritmo_Menstrual }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> FUM: {{ $paciente->FUM }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Gestas: {{ $paciente->Gestas }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Abortos: {{ $paciente->Abortos }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Cesareas: {{ $paciente->Cesareas }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> FUP: {{ $paciente->FUP }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> USA: {{ $paciente->USA }}</p>
            </div>
               <div class="form-group">
                   <p class="card-text"> Uso de Anticonseptivos: {{ $paciente->Anticonseptivos }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Paras: {{ $paciente->Paras }}</p>
               </div>
           </div>
         </div>

       <div class="card text-white bg-dark mb-3" >
           <div class="card-header"> Exploracion</div>
           <div class="card-body">
            <div class="form-group">
                <p class="card-text"> Peso: {{ $paciente->Peso }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Talla: {{ $paciente->Talla }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Cabeza: {{ $paciente->Cabeza }}</p>
            </div>
            <div class="form-group">
                <p class="card-text"> Ojos: {{ $paciente->Ojos }}</p>
            </div>
                   <div class="form-group">
                       <p class="card-text"> Oidos: {{ $paciente->Oidos }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Nariz: {{ $paciente->Nariz }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Torax: {{ $paciente->Torax }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Forma: {{ $paciente->Forma }}</p>
                   </div>

               <div class="form-group">
                   <p class="card-text"> Boca: {{ $paciente->Boca }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Cuello: {{ $paciente->Cuello }}</p>
               </div>
                   <div class="form-group">
                       <p class="card-text"> Moviemientos Respiratorios: {{ $paciente->Mov_Respira }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> FR: {{ $paciente->FR }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Ruidos Anormales: {{ $paciente->Ruido_Anormal }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> FC: {{ $paciente->FC }}</p>
                   </div>
               <br>
               <div class="form-group">
                   <p class="card-text"> Abdomen: {{ $paciente->Abdomen }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Extremidades Superiores: {{ $paciente->Extre_superior }}</p>
               </div>
               <div class="form-group">
                   <p class="card-text"> Extremidades Inferiores: {{ $paciente->Extre_inferior }}</p>
               </div>
           </div>
         </div>
   </div>
       <!-- <div class="form-group">
            <br>
            <div class="card text-white bg-dark mb-3" >
               <div class="card-header">Datos Generales</div>
               <div class="card-body">
                   <div class="form-group">
                       <p class="card-text"> Nombre: {{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}} </p>
                   </div>
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-2 mx-auto">
                           <p class="card-text"> Edad: {{$paciente->Edad}}</p>
                       </div>
                       <div class="col-md-10 mx-auto">
                           <p class="card-text"> Direccion: {{ $paciente->Direccion }}</p>
                       </div>
                   </div>
               </div>
             </div>
           <br>
           <div class="card text-white bg-dark mb-3" >
               <div class="card-header"> Antecedentes heredofamiliares</div>
               <div class="card-body">
                   <div class="form-group">
                       <p class="card-text"> _Abuelos Paternos: {{ $paciente->Ante_AbuelosP }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Abuelos Maternos: {{ $paciente->Ante_AbuelosM }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Tios: {{ $paciente->Ante_Tios }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Mama: {{ $paciente->Ante_Mama }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Papa: {{ $paciente->Ante_Papa }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Hermanos: {{ $paciente->Ante_Hermanos }}</p>
                   </div>
               </div>
             </div>
           <div class="card text-white bg-dark mb-3" >
               <div class="card-header"> Antecedentes Personales No Patologicos</div>
               <div class="card-body">
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Fecha Nacimiento: {{ $paciente->Fecha_Nacimiento }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Lugar Nacimiento: {{ $paciente->Lugar_Nacimiento }}</p>
                       </div>
                       @if ($paciente->Estado_Civil == "Soltero")
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Estado Civil: Soltero </p>
                       </div>
                       @elseif($paciente->Estado_Civil == "Casado")
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Estado Civil: Casado </p>
                       </div>
                       @else
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Estado Civil: Viudo </p>
                       </div>
                       @endif
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Toxicomanias: {{ $paciente->Toxicomanias }}</p>
                       </div>
                   </div>
                   <br>
                   <div class="form-group">
                       <p class="card-text"> _Residencia Actual: {{ $paciente->Residencia_actual }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Escolaridad: {{ $paciente->Escolaridad }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Habitos de Higiene: {{ $paciente->Habitos_Higiene }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> _Habitos de Dieta: {{ $paciente->Habitos_Dieta }}</p>
                   </div>
               </div>
             </div>
           <div class="card text-white bg-dark mb-3" >
               <div class="card-header"> Antecedentes Personales Patologicos</div>
               <div class="card-body">
                   <div class="form-group">
                       <p class="card-text"> Enfermedad de la Infancia: {{ $paciente->Enferme_infancia }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Cirugias: {{ $paciente->Cirugias }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Alergias: {{ $paciente->Alergias }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Medicamento que toma Actualmente: {{ $paciente->Medica_Actual }}</p>
                   </div>
               </div>
             </div>
           <div class="card text-white bg-dark mb-3" >
               <div class="card-header"> Antecedentes Gineco Obstetricos</div>
               <div class="card-body">
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Menarca: {{ $paciente->Menarca }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Ritmo Menstrual: {{ $paciente->Ritmo_Menstrual }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> FUM: {{ $paciente->FUM }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Gestas: {{ $paciente->Gestas }}</p>
                       </div>
                   </div>
                   <br>
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Abortos: {{ $paciente->Abortos }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Cesareas: {{ $paciente->Cesareas }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> FUP: {{ $paciente->FUP }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> USA: {{ $paciente->USA }}</p>
                       </div>
                   </div>
                   <br>
                   <div class="form-group">
                       <p class="card-text"> Uso de Anticonseptivos: {{ $paciente->Anticonseptivos }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Paras: {{ $paciente->Paras }}</p>
                   </div>
               </div>
             </div>

           <div class="card text-white bg-dark mb-3" >
               <div class="card-header"> Exploracion</div>
               <div class="card-body">
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Peso: {{ $paciente->Peso }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Talla: {{ $paciente->Talla }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Cabeza: {{ $paciente->Cabeza }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Ojos: {{ $paciente->Ojos }}</p>
                       </div>
                   </div>
                   <br>
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Oidos: {{ $paciente->Oidos }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Nariz: {{ $paciente->Nariz }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Torax: {{ $paciente->Torax }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Forma: {{ $paciente->Forma }}</p>
                       </div>
                   </div>
                   <br>
                   <div class="form-group">
                       <p class="card-text"> Boca: {{ $paciente->Boca }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Cuello: {{ $paciente->Cuello }}</p>
                   </div>
                   <div class="justify-content-between align-items-center row">
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Moviemientos Respiratorios: {{ $paciente->Mov_Respira }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> FR: {{ $paciente->FR }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> Ruidos Anormales: {{ $paciente->Ruido_Anormal }}</p>
                       </div>
                       <div class="col-md-3 mx-auto">
                           <p class="card-text"> FC: {{ $paciente->FC }}</p>
                       </div>
                   </div>
                   <br>
                   <div class="form-group">
                       <p class="card-text"> Abdomen: {{ $paciente->Abdomen }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Extremidades Superiores: {{ $paciente->Extre_superior }}</p>
                   </div>
                   <div class="form-group">
                       <p class="card-text"> Extremidades Inferiores: {{ $paciente->Extre_inferior }}</p>
                   </div>
               </div>
             </div>
       </div>-->
</body>
</html>
