@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>
                <div class="card-body">
                    <form action="{{route('tiene.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="rol_id" class="col-md-4 col-form-label text-md-right ">{{ __(' Rol') }}</label>
                            <input type="hidden" name="id"value="{{$user->id}}" id="{{$user->id}}">
                            <div class="col-md-6">
                                @foreach ($roles as $item)
                                    <p>
                                        <input type="checkbox" name="id_rol[]" value="{{$item->id}}" class="field-in" id="{{$item->id}}" @if($user->has_role($item->Nombre_rol))checked @endif/>
                                        <label for="{{$item->id}}"></label>
                                        <span>{{$item->Nombre_rol}}</span>
                                    </p>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                <a class="btn btn-outline-danger" href="{{route('usuarios.index')}}">Regresar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
