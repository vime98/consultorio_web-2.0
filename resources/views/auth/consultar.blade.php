@extends('layouts.app')

@section('content')


<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Mis Usuarios</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('register') }}"> Agregar Nuevo Usuario</a>
        </div>
        <br>
    </div>
</div>
<br>
<div class="table-responsive">
  <input class="form-control mb-4" id="tableSearch2" type="text" placeholder="Type something to search list items"></div>
    <table id="dtBasicExample" class="table table-striped table-dark" cellspacing="0" width="100%">
  <thead>
      <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Email</th>
          <th>Apellidos</th>
          <th>Accion</th>

      </tr>
  </thead>
  <tbody id="myDIV">
      @forelse ($users as $item)
      <tr>
        <td>{{$loop->iteration }}</td>
        <td>{{ $item->Nombre}}</td>
        <td>{{ $item->email }}</td>
        <td>{{ $item->Apellido_Paterno}} {{ $item->Apellido_Materno}}</td>
      <td>
            {{-- <a class="btn btn-warning" data-toggle="modal" data-target="#viewData{{$item->id}}">Ver</a> --}}
            <a class="btn btn-success" href="{{ route('tiene.edit', $item->id) }}">Roles</a>
            <a class="btn btn-primary" href="{{ route('usuarios.edit',$item->id) }}">Editar</a>
            <a class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop{{$item->id}}" >Eliminar</a>
    </td>
    </tr>
      @empty

      @endforelse
  </tbody>
  </table>
  </div>

  @forelse ($users as $item)
  <div class="modal fade" id="staticBackdrop{{$item->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('usuarios.destroy',$item->id) }}" method="POST">
            @csrf
            @method('DELETE')
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">¿Seguro que quiere eliminar a este paciente: {{$item->Nombre}} {{$item->Apellido_Paterno}} {{$item->Apellido_Materno}} ?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <img img class="card-img-top" src="img/Delete.jpg"
            alt="Card image cap">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancerlar</button>
          <button type="submit" class="btn btn-danger">Confirmar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  @empty
  @endforelse

  @forelse ($users as $item)
  <div class="modal fade" id="viewData{{$item->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="viewDataLabel">Paciente: {{$item->nombre}} {{$item->a_paterno}} {{$item->a_materno}} </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p><Strong><h3>Nombre Completo:</h3></Strong> {{$item->Nombre}} {{$item->Apellido_Paterno}} {{$item->Apellido_Materno}}</p>
            <p><Strong><h3>Correo:</h3></Strong> {{$item->email}} </p>
            <p><Strong><h3>Doctor Principal :</h3></Strong> {{ $item->Nombre }}</p>
            <p><Strong><h3>Fecha y hora de creacion :</h3></Strong> {{ $item->created_at }}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  @empty

  @endforelse
  <!-- Modal -->


@endsection
