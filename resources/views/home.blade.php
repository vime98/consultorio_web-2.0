@extends('layouts.app')
@section('content')
<div class="container">
  
    <div class="container my-5">

        <!-- Section: Block Content -->
        <section >
          
      
          <!-- Grid row -->
          <div class="row"  >
      
            <!-- Grid column -->
            <div class="col-12" >
      
              <div class="view zoom z-depth-1 rounded mb-4" >
                <a href="{{ route('paciente.index') }}">
                  <img src="images/Medicina.jpg" class="img-fluid" alt="sample image" style="width: 2000px; height: 500px;" >
                  <div class="mask rgba-black-gradient d-md-flex align-items-end">
                    <div class="text-bottom white-text p-4">
                      <h3 class="card-title font-weight-bold mt-2 mb-1"> Bienvenido Dr {{Auth::guard('web')->user()->Nombre}}</h3>
                      <button class="btn btn btn-primary btn-sm btn-rounded mx-0 mb-0 d-none d-sm-inline-block"> Ver mis pacientes</button>
                    </div>
                  </div>
                </a>
              </div>
      
            </div>
            <!-- Grid column -->
            
          </div>
          <!-- Grid row -->
      
        </section>
        <!-- Section: Block Content -->
      
      </div>

      <div class="container my-5">

        <!-- Section: Block Content -->
        <section >
          
      
          <!-- Grid row -->
          <div class="row"  >
      
            <!-- Grid column -->
            <div class="col-12" >
      
              <div class="view zoom z-depth-1 rounded mb-4" >
                <a href="{{ route('notas.create') }}">
                  <img src="images/Notas.jpg" class="img-fluid" alt="sample image" style="width: 2000px; height: 500px;" >
                  <div class="mask rgba-black-gradient d-md-flex align-items-end">
                    <div class="text-bottom white-text p-4">
                      <h3 class="card-title font-weight-bold mt-2 mb-1"> Notas</h3>
                      <button class="btn btn btn-primary btn-sm btn-rounded mx-0 mb-0 d-none d-sm-inline-block"> Dejar una Nota</button>
                    </div>
                  </div>
                </a>
              </div>
      
            </div>
            <!-- Grid column -->
            
          </div>
          <!-- Grid row -->
      
        </section>
        <!-- Section: Block Content -->
      
      </div>
</div>
@endsection
