<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">
    <style>
        body{
            background-image: url('images/Login.png');
            height: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            .alert{
                z-index: 99;
                top: 60px;
                right:18px;
                min-width:30%;
                position: fixed;
                animation: slide 0.5s forwards;
            }
            @keyframes slide {
                100% { top: 30px; }
            }
            @media screen and (max-width: 668px) {
                .alert{ /* center the alert on small screens */
                    left: 10px;
                    right: 10px;
                }
            }
        }
    </style>    
</head>
        /* these styles will animate bootstrap alerts. */
        
    
<body>

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ config('', 'Hospital K-Paz') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @if(Auth::guard('web')->check())
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ _('Usuarios') }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('usuarios.index') }}">
                                    {{ __('Consultar') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('register') }}">
                                    {{ __('Crear') }}
                                </a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ _('Notas') }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('notas.index') }}">
                                    {{ __('Ver Notas') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('notas.create') }}">
                                    {{ __('Crear') }}
                                </a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ _('Doctor') }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('paciente.index') }}">
                                    {{ __('Ver Mis Pacientes') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('paciente.create') }}">
                                    {{ __('Agregar un Paciente') }}
                                </a>
                            </div>
                        </li>
                        @elseif(Auth::guard('paciente')->check())
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ _('Paciente') }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('VerMiExpediente',Auth::guard('paciente')->user()->id)}}">
                                    {{ __('Ver mi Expediente') }}
                                </a>
                            </div>
                        </li>
                         @else

                      @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @if(Auth::guard('web')->check())
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   Dr: {{ Auth::guard('web')->user()->Nombre }} (@foreach ( Auth::guard('web')->user()->roles as $item)
                                   {{$item->Nombre_rol}} @endforeach ) <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a href="{{route('home')}}" class="dropdown-item">Home (Casa)</a>
                                    <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @elseif(Auth::guard('paciente')->check())
                            <li class="nav-item dropdown">
                                <a id="pacienteDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::guard('paciente')->user()->Nombre }} (paciente) <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="pacienteDropdown">
                                    <a href="{{route('paciente.home')}}" class="dropdown-item">Home (Casa)</a>
                                    <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#paciente-logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="paciente-logout-form" action="{{ route('paciente.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @else
                           
                        @endif
                    </ul>
                </div>

            </div>
        </nav>
        {{-- Success Alert --}}
    @if(session('status'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session('status')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

{{-- Error Alert --}}
@if(session('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{session('error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

        <main class="container">
            <br>
            @yield('content')
        </main>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>
    <script>
        $(document).ready(function () {
        $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');
        });
    </script>
    <script>
        $(document).ready(function () {
        $("#tableSearch2").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myDIV tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
        $(document).ready(function () {
        $("#tableSearch3").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myDIV div").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
    </script>
</body>
</html>
