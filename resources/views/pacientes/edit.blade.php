@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Editar Paciente {{$users->name}}</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('paciente.index') }}"> Atras</a>
        </div>
    </div>
</div>

<form action="{{ route('paciente.update', $users->id ) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <div class="justify-content-between align-items-center row">
            <div class="col-md-12 mx-auto">
                <label for="Nombre"><strong>{{'Nombre(s) '}}</strong></label>
                <input type="text" name="Nombre" id="Nombre" placeholder="Nombre" class="form-control input-sm" pattern="[a-z0-9A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð #,.'-]{3,23}"
                value="{{ $users->Nombre }}" required>
            </div>
        </div>
        <br>
        <div class="justify-content-between align-items-center row">
            <div class="col-md-4 mx-auto">
                <label for="Apellido_Paterno"><strong>{{'Apellido Paterno '}}</strong></label>
                <input type="text" name="Apellido_Paterno" id="Apellido_Paterno" placeholder="Apellido Paterno" class="form-control input-sm" pattern="[a-z0-9A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð #,.'-]{3,23}"
                value="{{ $users->Apellido_Paterno }}" required>
            </div>
            <div class="col-md-4 mx-auto">
                <label for="Apellido_Materno"><strong>{{'Apellido Materno '}} </strong></label>
                <input type="text" name="Apellido_Materno" id="Apellido_Materno" placeholder="Apellido Materno" class="form-control input-sm" pattern="[a-z0-9A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð #,.'-]{3,23}"
                value="{{ $users->Apellido_Materno }}">
            </div>
            <div class="col-md-4 mx-auto">
                @if ($users->Sexo == "M")
                <label for="Sexo"><strong>{{'Sexo'}}</strong></label>
                <select id="Sexo"  class="form-control" name="Sexo" autofocus>
                    <option value="M">Masculino</option>
                    <option value="F">Femenino</option>
                </select>
                @else
                <label for="Sexo"><strong>{{'Sexo'}}</strong></label>
                <select id="Sexo"  class="form-control" name="Sexo" autofocus>
                    <option value="F">Femenino</option>
                    <option value="M">Masculino</option>
                </select>
                @endif
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="form-group">
                <strong>Correo:</strong>
                <input type="text" name="email" class="form-control" placeholder="Correo" value="{{ $users->email }}" required>
            </div>
        </div>
        <div class="form-group">
            @if ($users->compartido == 1)
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="compartido" name="compartido" checked >
                <label class="custom-control-label" for="compartido">Compartir Paciente</label>
            </div>
            @else
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="compartido" name="compartido">
                <label class="custom-control-label" for="compartido">Compartir Paciente</label>
            </div>
            @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</div>

</form>
@endsection
