@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('paciente.index') }}"> Atras</a>
        </div>
    </div>
</div>
<div class="container">
    <p><h3>Nombre Completo:</h3> {{$paciente->Nombre}} {{$paciente->Apellido_Paterno}} {{$paciente->Apellido_Materno}}</p>
    <p><h3>Correo:</h3> {{$paciente->email}} </p>
    <p><h3>Confirmado:</h3> {{$paciente->compartido}}</p>
    <p><h3>Usuario Relacionado :</h3> {{$paciente->user_id}}</p>
</div>
@endsection
